

$("input[name=senha]").focusin(() => {
    $(".boneca").css("display", "none");
    $(".mao").css("display", "none");
    $(".senha").css("display", "block");
});

$("input[name=senha]").focusout(() => {
    $(".boneca").css("display", "block");
    $(".mao").css("display", "block");
    $(".senha").css("display", "none");
});